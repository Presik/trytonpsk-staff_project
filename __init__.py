# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
# from . import contract
# from . import employee
# from . import liquidation
# from . import work
# from . import payroll


def register():
    Pool.register(
        # employee.Employee,
        # contract.Contract,
        # contract.ContractingFicStart,
        # liquidation.Liquidation,
        # work.Work,
        # payroll.Payroll,
        # payroll.PayrollPaycheckStart,
        # payroll.PayrollSheetStart,
        # payroll.PayrollSheetConvencionalStart,
        # payroll.PayrollGlobalStart,
        module='staff_project', type_='model')
    Pool.register(
        # contract.ProjectEmployeeContractReport,
        # contract.ProjectHistoryReport,
        # contract.ProjectDiscountReport,
        # contract.ProjectNoCertificationReport,
        # contract.ContractingFicReport,
        # payroll.PayrollPaycheckReport,
        # payroll.PayrollSheetConvencionalReport,
        # payroll.PayrollGlobalReport,
        module='staff_project', type_='report')
    Pool.register(
        # contract.ContractingFic,
        # payroll.PayrollPaycheck,
        # payroll.PayrollSheet,
        # payroll.PayrollSheetConvencional,
        # payroll.PayrollGlobal,
        # payroll.PayrollGroup,
        module='staff_project', type_='wizard')
